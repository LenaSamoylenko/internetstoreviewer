﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace InternetStoreViewer.Models
{
    public class SiteUrl : IEntity, IChangeProp<SiteUrl>
    {
        public int ID { get; set; }

        [Display(Name = "Address")]
        [Required]
        public string Url { get; set; }
        public Host Hosts { get; set; }
       

        public void ChangeProp(SiteUrl newEnt, ref SiteUrl entity)
        {
            if (newEnt.ID == entity.ID)
            {
                entity.Url = newEnt.Url;
                entity.Hosts = newEnt.Hosts;
            }
        }
    }
}