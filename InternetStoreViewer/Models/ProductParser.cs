﻿using AngleSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace InternetStoreViewer.Models
{
    public enum TypeOfAsync
    {
        Path,
        Text
    }

    public class GetterProductsFromPage
    {
        private string _path = null;
        private readonly ProductParser _parser = null;
        private CSS_Classes _styleClass = null;
        private int _categoryID = 0;


        public GetterProductsFromPage(string path)
        {
            _parser = new ProductParser();
            _path = path;
        }

        public async Task<IEnumerable<Product>> GetProductFromPage(CSS_Classes styleClass)
        {
            try
            {
                List<Product> _products = new List<Product>();
                Category _category = new Category() { Name = GetCategory(styleClass.CategoryClass) };

                //Get string collection with catalog with products
                using (var _allProductsInCatalogs = _parser.GetEntitiesText(_path, styleClass.CatalogClass, TypeOfAsync.Path))
                {
                    Parallel.ForEach(
                        source: _allProductsInCatalogs.Result, body: (item) => { _products.Add(GetProduct(item).Result); });

                    return _products;
                }

            }
            catch (Exception)
            {

                throw;
            }

        }

        public string GetCategory(string cssCategoryClass)
        {
            return _parser.GetEntitiesText(_path, cssCategoryClass, TypeOfAsync.Path).Result.FirstOrDefault();
        }

        public async Task<Product> GetProduct(string htmlCellString)
        {
            using (var _product = new Product())
            {
                try
                {
                    _product.Name = (await _parser.GetEntitiesText(htmlCellString, _styleClass.NameClass, TypeOfAsync.Text)).FirstOrDefault();

                    (_product.Photo, _product.ImageType) = await GetImg(htmlCellString, _styleClass.ImageClass);

                    _product.Description = (await _parser.GetEntitiesText(htmlCellString, _styleClass.DescriptionClass, TypeOfAsync.Text)).FirstOrDefault();

                    _product.Price = Decimal.Parse((await _parser.GetEntitiesText(htmlCellString, _styleClass.PriceClass, TypeOfAsync.Text)).FirstOrDefault());

                    _product.CategoryID = _categoryID;
                }
                catch (Exception)
                {
                    throw;
                }

                return _product;
            }
        }

        private async Task<(byte[], string)> GetImg(string htmlString, string cssImgClass)
        {
            using (var _imgPathsTask = _parser.GetEntitiesText(htmlString, cssImgClass, TypeOfAsync.Text))
            {
                var _imgPath = (await _imgPathsTask).FirstOrDefault();

                var _imgType = _imgPath.Split('.').FirstOrDefault();

                var _imgArr = ProductParser.GetFileBytes(_imgPath);

                return (_imgArr, _imgType);
            }
        }

    }

    public class ProductParser
    {
        private IConfiguration _configuration;

        public ProductParser()
        {
            _configuration = Configuration.Default.WithDefaultLoader();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="param">For put pages URL or string text for search</param>
        /// <param name="pattern">For find component in HTML</param>
        /// <returns></returns>
        public async Task<IEnumerable<string>> GetEntitiesText(string param, string pattern, TypeOfAsync typeOpenAsync)
        {
            AngleSharp.Dom.IDocument document = null;

            // Load the names of all The Big Bang Theory episodes from Wikipedia
            var context = BrowsingContext.New(_configuration);

            // Asynchronously get the document in a new context using the configuration
            switch (typeOpenAsync)
            {
                case TypeOfAsync.Path:
                    document = await context.OpenAsync(param);
                    break;

                case TypeOfAsync.Text:
                    document = await context.OpenAsync(req => req.Content(param));
                    break;

                default:
                    break;
            }

            // Perform the query to get all cells with the content
            var cells = document.QuerySelectorAll(pattern);


            //// We are only interested in the text - select it with LINQ
            var title = cells.Select(m => m.InnerHtml);

            return title;
        }


        public static byte[] GetFileBytes(string path)
        {
            FileStream fileOnDisk = new FileStream(HttpRuntime.AppDomainAppPath + path, FileMode.Open);
            byte[] fileBytes;

            lock (new object())
            {
                using (BinaryReader br = new BinaryReader(fileOnDisk))
                {
                    fileBytes = br.ReadBytes((int)fileOnDisk.Length);
                }
            }

            return fileBytes;
        }
    }
}