﻿namespace InternetStoreViewer.Models
{
    public interface IChangeProp<T> where T:class, IEntity
    {
        void ChangeProp(T newEnt, ref T entity);
    }
}