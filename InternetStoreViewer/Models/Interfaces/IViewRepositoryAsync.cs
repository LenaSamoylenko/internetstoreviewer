﻿using System.Threading.Tasks;

namespace InternetStoreViewer.Models
{
    interface IViewRepositoryAsync<T> where T : IEntity
    {
        Task<T> GetEntityByIDAsync(int ID);
        Task CreateAsync(T model);
        Task<bool> UpdateAsync(T model);
        Task SaveChangesAsync();
    }
}