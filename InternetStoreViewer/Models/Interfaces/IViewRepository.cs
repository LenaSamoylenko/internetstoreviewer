﻿using System.Linq;

namespace InternetStoreViewer.Models
{
    interface IViewRepository<T> where T : IEntity
    {
        IQueryable<T> GetAll();
        T GetEntityByID(int ID);
        void Create(T model);
        bool Update(T model);
        bool Delete(T model);
        void SaveChanges();
    }
}