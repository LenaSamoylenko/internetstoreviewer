﻿using System.ComponentModel.DataAnnotations;

namespace InternetStoreViewer.Models
{
    public interface IEntity
    {
        [Key]
        int ID { get; set; }
    }
}