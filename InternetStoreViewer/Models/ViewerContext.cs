﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Web;

namespace InternetStoreViewer.Models
{
    public class ViewerContext : DbContext
    {
        public DbSet<SiteUrl> Sites { get; set; }
        public DbSet<Host> UserRoutes { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Product> Produces { get; set; }

    }
}