﻿using System.Collections.Generic;

namespace InternetStoreViewer.Models
{
    public class Host : IEntity, IChangeProp<Host>
    {
        public int ID { get; set; }
        public string Name { get; set; }

        public virtual SiteUrl SiteUrl { get; set; }
        public virtual List<Category> Categories { get; set; }


        public void ChangeProp(Host newEnt, ref Host entity)
        {
            if (newEnt.ID == entity.ID)
            {
                entity.Name = newEnt.Name;
                entity.SiteUrl = newEnt.SiteUrl;
            }
        }
    }
}