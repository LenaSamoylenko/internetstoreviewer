﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace InternetStoreViewer.Models
{
    public class ViewerRepositoryAsync<T> : IViewRepositoryAsync<T>, IViewRepository<T> where T : class, IEntity, IChangeProp<T>
    {
        private readonly ViewerContext _context = null;

        public ViewerRepositoryAsync()
        {
            _context = new ViewerContext();
        }

        public ViewerRepositoryAsync(ViewerContext context)
        {
            _context = context;
        }

        public async Task CreateAsync(T model)
        {
            _context.Set<T>().Add(model);

            await SaveChangesAsync();
        }

        public bool Delete(T model)
        {
            try
            {
                var t = _context.Set<T>().Remove(model);
            }
            catch (Exception)
            {

                return false;
            }

            return true;
        }

        public IQueryable<T> GetAll()
        {
            return _context.Set<T>().AsQueryable();
        }

        public T GetEntityByID(int ID)
        {
            return _context.Set<T>().FirstOrDefault(f => f.ID == ID);
        }

        public async Task<T> GetEntityByIDAsync(int ID)
        {
            return await _context.Set<T>().FirstOrDefaultAsync(f => f.ID == ID);
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }

        public async Task SaveChangesAsync()
        {
            await _context.SaveChangesAsync();
        }

        public bool Update(T model)
        {
            try
            {
                T Entity = _context.Set<T>().Where(p => p.ID == model.ID).FirstOrDefault();
                Entity.ChangeProp(model, ref Entity);

                SaveChanges();
            }
            catch (Exception)
            {

                return false;
            }

            return true;
        }

        public async Task<bool> UpdateAsync(T model)
        {
            try
            {
                T Entity = await _context.Set<T>().Where(p => p.ID == model.ID).FirstOrDefaultAsync();
                Entity.ChangeProp(model, ref Entity);

                await SaveChangesAsync();
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        void IViewRepository<T>.Create(T model)
        {
            _context.Set<T>().Add(model);

            SaveChanges();
        }
    }
}