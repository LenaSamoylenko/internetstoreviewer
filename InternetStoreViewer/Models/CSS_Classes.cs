﻿namespace InternetStoreViewer.Models
{
    public class CSS_Classes
    {
        public int ID { get; set; }
        public string CatalogClass { get; set; }
        public string NameClass { get; set; }
        public string ImageClass { get; set; }
        public string DescriptionClass { get; set; }
        public string PriceClass { get; set; }
        public string CategoryClass { get; set; }

        public virtual Host Host { get; set; }
    }
}