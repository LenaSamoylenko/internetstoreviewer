﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InternetStoreViewer.Models
{
    public class Category:IEntity, IChangeProp<Category>
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public virtual List<Product> Produces { get; set; }
        public virtual int SiteID { get; set; }

        public void ChangeProp( Category newEnt, ref Category  entity)
        {
            if (entity.ID==newEnt.ID)
            {
                entity.Name = newEnt.Name;
                entity.Produces = newEnt.Produces;
                entity.SiteID = newEnt.SiteID;
            }
        }
    }
}