﻿using System;

namespace InternetStoreViewer.Models
{
    public class Product : IEntity, IChangeProp<Product>, IDisposable
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public byte[] Photo { get; set; }
        public string ImageType { get; set; }
        public string Description { get; set; }
        public decimal? Price { get; set; }

        public virtual Category Category { get; set; }
        public virtual int? CategoryID { get; set; }

        public void ChangeProp(Product newEnt, ref Product entity)
        {
            if (entity.ID == newEnt.ID)
            {
                entity.Name = newEnt.Name;

                if (entity.Photo.GetHashCode() != newEnt.Photo.GetHashCode())
                {
                    entity.Photo = null;

                    entity.Photo = newEnt.Photo;
                }

                entity.Description = newEnt.Description;
            }
        }

        public void Dispose()
        {
            GC.Collect();
        }
    }
}