﻿using InternetStoreViewer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace InternetStoreViewer.Controllers
{
    public class SiteController : Controller
    {
        private ViewerRepositoryAsync<SiteUrl> _sitesRepo;

        public SiteController()
        {
            _sitesRepo = new ViewerRepositoryAsync<SiteUrl>();
        }

        public SiteController(ViewerRepositoryAsync<SiteUrl> repo)
        {
            _sitesRepo = repo;
        }

        [HttpPost]
        public async Task<ActionResult> Create(SiteUrl site)
        {
            if (!ModelState.IsValid)
            {
                return View("_ForPutAddress");
            }
            else
            {
                var sitesFromDb = _sitesRepo.GetAll().FirstOrDefault(s => s.Url == site.Url);

                if (sitesFromDb == null)
                {
                    await this.SetSiteToDB(site);
                }

                RedirectToAction("GetEntities", "Product", site.Url);
            }

            return null;
        }

        private async Task SetSiteToDB(SiteUrl site)
        {
            await _sitesRepo.CreateAsync(site);
        }

        [ChildActionOnly]
        public async Task ParsePageOnProducts(int siteID)
        {

        }

        [ChildActionOnly]
        public ActionResult _ForPutAddress()
        {
            return PartialView("_ForPutAddress");
        }

    }
}