﻿using InternetStoreViewer.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace InternetStoreViewer.Controllers
{
    enum Roles
    {
        Admin,
        User
    }

    [HandleError(View = "Error")]
    public class ProductController : Controller
    {
        private ViewerRepositoryAsync<Product> _productsRepo;
        private int _pageSize = 5;

        public int PageSize { get => _pageSize; set => _pageSize = value; }

        public ProductController()
        {
            _productsRepo = new ViewerRepositoryAsync<Product>();
        }

        public ProductController(ViewerRepositoryAsync<Product> repo1, ViewerRepositoryAsync<SiteUrl> repo2)
        {
            _productsRepo = repo1;
        }


        // GET: Product
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }


        // GET: Product/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Product/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Product/Create
        [HttpPost]
        [Authorize(Roles = nameof(Roles.Admin))]
        public async Task<ActionResult> Create(Product product)
        {
            try
            {
                // TODO: Add insert logic here
                await _productsRepo.CreateAsync(product);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }


        // GET: Product/Edit/5
        //[Authorize(Roles = nameof(Roles.Admin))]
        //public ActionResult Edit(int id)
        //{
        //    return View();
        //}

        // POST: Product/Edit/5
        [HttpPost]
        [Authorize(Roles = nameof(Roles.Admin))]
        public async Task<ActionResult> Edit(int id)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //// GET: Product/Delete/5
        //[Authorize(Roles = nameof(Roles.Admin))]
        //public ActionResult Delete(Product product)
        //{
        //    return View();
        //}

        // POST: Product/Delete/5
        [HttpPost]
        [Authorize(Roles = nameof(Roles.Admin))]
        //TODO: why for  "FormCollection collection"
        public async Task<ActionResult> Delete(Product product)
        {
            try
            {
                _productsRepo.Delete(product);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        [ChildActionOnly]
        public ViewResult _Products(string category, int pageSize, int page = 1)
        {
            _pageSize = pageSize;

            List<Product> products = _productsRepo.GetAll()
                .Where(c => c.CategoryID == null || c.Category.Name == category)
                .OrderBy(p => p.ID)
                .Skip((page - 1) * PageSize)
                .Take(PageSize)
                .ToList();

            return View(products);
        }

        public async Task<ActionResult> GetEntities(string path)
        {
            GetterProductsFromPage _prodClass = new GetterProductsFromPage(path);

            IEnumerable<Product> _allEntitiesAfterParsing = new List<Product>();

            foreach (var ent in _allEntitiesAfterParsing)
            {
                await _productsRepo.CreateAsync(ent);
            }

            return View("ProductList", _allEntitiesAfterParsing);

        }


        public async Task<FileContentResult> GetImage(int productID)
        {
            Product _product = await _productsRepo.GetEntityByIDAsync(productID);

            if (_product != null)
            {
                return File(_product.Photo, _product.ImageType);
            }
            else
            {
                return null;
            }
        }


    }
}
